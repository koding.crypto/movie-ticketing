import javax.swing.*;
import java.awt.*;

public class MainPage extends JFrame{
    private JPanel basePanel;
    private JPanel headerPanel;
    private JPanel bodyPanel;
    private JPanel footerPanel;
    private JButton verifikasiButton;
    private JTextField namaField;
    private JRadioButton studio1RadioButton;
    private JRadioButton studio4RadioButton;
    private JRadioButton studio3RadioButton;
    private JRadioButton studio2RadioButton;
    private JButton pesanTiketButton;
    private JButton keluarButton;
    private JButton beliTiketButton;
    private JTextField judulFilmField;
    private JComboBox jamTayangComboBox;
    private JComboBox hariComboBox;
    private JComboBox noKursiComboBox;
    private JTextField jumlahTiketField;
    private JTextField totalHargaField;
    private JTable table1;
    private JButton hapusButton;
    private JLabel hargaSeninKamisLabel;
    private JLabel hargaJumatLabel;
    private JLabel hargaSabtuMingguLabel;

    private static final int hargaSeninKamis    = 30000;
    private static final int hargaJumat         = 35000;
    private static final int hargaSabtuMinggu   = 45000;

    public MainPage(String title) throws HeadlessException {
        super(title);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(basePanel);
        this.pack();

        hargaSeninKamisLabel.setText(Integer.toString(hargaSeninKamis));
        hargaJumatLabel.setText(Integer.toString(hargaJumat));
        hargaSabtuMingguLabel.setText(Integer.toString(hargaSabtuMinggu));

        jamTayangComboBox.setSelectedItem("Pilih Jam");

    }

    public static void main(String[] args) {
        JFrame jFrame = new MainPage("Movie Ticketing");
        jFrame.setSize(500, 700);
        jFrame.setVisible(true);
    }
}
